from django.db import models
from django.contrib.auth.models import User
from django import forms
    
class UserProf(forms.ModelForm):
        class Meta:
            model = User
            fields = '__all__'