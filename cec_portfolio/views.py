from django.http import HttpResponse
from django.shortcuts import render, redirect 
from django.contrib.auth import authenticate, login
from django.contrib.auth import logout as django_logout
from django.contrib.auth.models import User

# Méthode / afficher Home.
def home(request):   
    return render(request, "home.html")


# # Méthode / afficher Projets.
# def projects(request):
#     return render(request, "projects.html")

# Méthode / afficher Articles.
def articles(request):
    return render(request, "articles.html")

# Méthode / afficher Formulaire de message Contact.
def contact(request):
    return render(request, "contact.html")

# def login(request):
#     return render(request, "login.html")
    
# def loginSubmit(request):
#     if request.method == 'POST':
#         email=request.POST.get('email')
#         password = request.POST.get("password")
#         try:
#           user = authenticate(username=username, password=password)
#         except:
#             message = "Identifiant et/ou mot de passe invalides."
#         return render(request,"login.html",{"msg":message})
#         print(user)
#     request.session['auth'] = True
#     request.session['user'] = email
#     return redirect("/")

# def register(request):
#     registered = False
#     if request.method == 'POST':
#         user_form = UserForm(data=request.POST)
#         if user_form.is_valid():
#             user = user_form.save()
#             user.set_password(user.password)
#             user.save()
#             registered = True
#         else:
#             print(user_form.errors)
#     else:
#         user_form = UserForm()
#     return render(request,'/register.html',
#                           {'user_form':user_form,
#                            'registered':registered})    

# def register(request):
#     return render(request, "register.html")

# def registerSubmit(request):
#     return render()                        