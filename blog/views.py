from django.shortcuts import render
from blog.models import Post

# Create your views here.

# Add post in Blog
# def add_post(request, pk):
#     form = PostForm()
#     if request.method == 'POST':
#         if form_is_valid():
#             post= Post(
#                 title = form.clean_data["title"],
#                 content = form.clean_data["content"],
#                 created_on = form.clean_data["created_on"],
#                 published = form.clean_data["published"],
#                 categories = form.clean_data["categories"]
#             )

# ReadAllPosts
def blog_index(request):
    posts = Post.objects.all().order_by('-created_on')
    context = {
        "posts" : posts,
    }
    return render(request, "blog_index.html", context)

def blog_category(request, category):
    posts = Post.objects.filter(
        categories__name__contains=category
    ).order_by(
        '-created_on'
    )
    context = {
        "category": category,
        "posts": posts
    }
    return render(request, "blog_category.html", context)

def blog_detail(request, id):
    post = Post.objects.get(id=id)
    context = {
        "posts":post,
    }

    return render(request, "blog_details.html", context)    
