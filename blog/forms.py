from django import forms
from blog.models import Post 
from django.contrib.auth.models import User

class PostForm(forms.ModelForm):
    class Meta():
        model = Post
        fields = ('title','content','created_on','published','categories')




