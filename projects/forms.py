from django import forms
from . import models
from projects.models import Project

class ProjectForm(forms.ModelForm):
    class Meta:
        model = Project
        fields = [ 
            "name", 
            "description", 
            "image",
            "repo_url",
            "website_url",
        ] 



     

