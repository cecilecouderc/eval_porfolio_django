from django.urls import path
from django.contrib import admin 
from . import views

urlpatterns = [
    path("", views.project_index, name="project_index"),
    path("<int:pk>/", views.project_detail, name="project_detail"),

# A renommer    
    path("add/", views.project_add, name="add"),
    path('show', views.show, name='show'),
    path("update/<int:id>", views.update, name="update"),
    path('delete/<int:id>', views.delete, name ="delete")
]

