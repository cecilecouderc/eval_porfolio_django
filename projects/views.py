from django.shortcuts import redirect, render
from django.http import HttpResponse, HttpResponseRedirect
from projects.models import Project
from projects.forms import ProjectForm
from django.contrib.auth.decorators import login_required

# Create your views here.

# Méthode pour afficher l'index de tous les projets
def project_index(request):
    projects = Project.objects.all()
    context = {
        'projects' : projects
    }
    return render(request, 'project_index.html', context)

# Méthode pour afficher le projet sélectionné 
def project_detail(request, id):
    project = Project.objects.get(id=id)
    context = {
        'project': project
    }
    return render(request, 'project_detail.html', context) 

# Methode pour ajouter un projet 
 
def project_add(request): 
    context ={} 

    form = ProjectForm(request.POST or None) 
    if form.is_valid(): 
        form.save()
        return redirect("/projects/show")  
          
    context['form']= form
    
    return render(request, "project_add.html", context) 

# Méthode pour accéder à tout le CRUD version admin  [test]
def show(request):
    projects = Project.objects.all()
    return render(request, 'show.html', {'projects': projects})

# Méthode pour mettre à jour un projet
def update(request, id):
    project = Project.objects.get(id=id) 
    form = ProjectForm(request.POST, instance=project)
    if form.is_valid():
        form.save()
        return redirect("/show")
    return render (request, 'project_update.html', {'project': project})

# Méthode pour supprimer un projet
def delete(request, id):
    project = Project.objects.get(id=id)
    project.delete()
    return redirect("/projects/")        