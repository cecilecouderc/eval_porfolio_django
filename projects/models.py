from django.db import models

# Create your models here.
# A déplacer
class Technology(models.Model):
    name = models.CharField(max_length=100, unique=True)
    
    def __str__(self):
        return self.name

class Project(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField()
    # image = models.FilePathField(path='projects/static/img/')  
    image = models.ImageField(upload_to='projects/static/img', null=True, blank=True) 
    repo_url = models.URLField(blank=False)
    website_url = models.URLField(blank=True)
    date = models.DateTimeField(auto_now_add=True)
    # technology = models.ManyToManyField(Technology, related_name='projects', blank=True)

    def __str__(self):
        return self.name

